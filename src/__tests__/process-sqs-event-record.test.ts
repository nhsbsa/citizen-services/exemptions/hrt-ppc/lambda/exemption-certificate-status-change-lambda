import { processRecord } from "../process-sqs-event-record";
import {
  mockSQSRecord,
  mockUserPreferenceRecord,
  mockNoCitizenId,
  mockNoCertificateId,
  mockNoCertificateType,
  mockNoCertificateStatus,
  mockNoCertificateReference,
  mockNoCertificateStartDate,
  mockNoCertificateEndDate,
  mockNotificationRecord,
} from "../__mocks__/mockData";
import { randomUUID } from "crypto";
import * as userPreference from "../api-utils/user-preference";
import * as notification from "../api-utils/notification";
import {
  Channel,
  Event,
  Preference,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest/dist/src/clients/common";
import { logger } from "@nhsbsa/hrt-ppc-npm-logging";

jest.mock("crypto");

let findUserPreferenceForCitizenSpy: jest.SpyInstance;
let sendNotificationPostSpy: jest.SpyInstance;

const mockRandomUUID = randomUUID as jest.MockedFunction<typeof randomUUID>;

const expectedUserPreferenceCall = `
  {
    "certificateType": "HRT_PPC",
    "citizenId": "citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx",
    "headers": {
      "channel": "BATCH",
      "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
      "user-id": "CERTIFICATE_EVENT",
    },
  }
`;

function buildExpectedNotificationCall(method?: string, channel?: string) {
  return `
    [
      "certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx",
      {
        "channel": \"${channel ?? "ONLINE"}"\,
        "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
        "user-id": "CERTIFICATE_EVENT",
      },
      {
        "certificateType": "HRT_PPC",
        "method": \"${method ?? "EMAIL"}"\,
        "purpose": "ISSUE",
      },
    ]
    `;
}

let sqsRecord;
let userPreferenceRecord;
let notificationRecord;
let winstonInfoLoggerSpy;

beforeEach(() => {
  sqsRecord = { ...mockSQSRecord };
  userPreferenceRecord = { ...mockUserPreferenceRecord };
  notificationRecord = { ...mockNotificationRecord() };

  findUserPreferenceForCitizenSpy = jest
    .spyOn(userPreference, "findUserPreferenceForCitizen")
    .mockResolvedValue(userPreferenceRecord);

  sendNotificationPostSpy = jest
    .spyOn(notification, "sendNotificationPost")
    .mockResolvedValue(notificationRecord);

  mockRandomUUID.mockReturnValue("a580305a-dae9-46a4-a420-ddee91054d7a");
  winstonInfoLoggerSpy = jest.spyOn(logger, "info");
});

afterEach(() => {
  jest.resetAllMocks();
  jest.restoreAllMocks();
  jest.resetModules();
});

describe("processRecord()", () => {
  afterEach(() => {
    // then / expect these logs for all scenarios
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      1,
      "Processing event [messageId: 059f36b4-87a3-44ab-83d2-661975830a7d]",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      2,
      "Validating SQS message attributes",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      3,
      "Received [eventName: certificateStatusChange, source: health-charge-exemption-certificate-api, eventType: issue-certificate correlationId: a580305a-dae9-46a4-a420-ddee91054d7a]",
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      4,
      "Validating record body",
    );
  });
  describe("with event as ANY and preference as EMAIL", () => {
    it.each([Channel.ONLINE, Channel.PHARMACY, Channel.STAFF])(
      "should process the sqs record and POST notification when EMAIL and channel %p",
      async (channel) => {
        // given
        userPreferenceRecord.preference = Preference.EMAIL;
        userPreferenceRecord._meta.channel = channel;

        // when
        await expect(processRecord(sqsRecord)).resolves.not.toThrowError();

        // then
        expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
        expect(
          findUserPreferenceForCitizenSpy.mock.calls[0][0],
        ).toMatchInlineSnapshot(expectedUserPreferenceCall);
        expect(sendNotificationPostSpy).toBeCalledTimes(1);
        expect(sendNotificationPostSpy.mock.calls[0]).toMatchInlineSnapshot(
          buildExpectedNotificationCall(Preference.EMAIL, channel),
        );
        expect(winstonInfoLoggerSpy).toBeCalledTimes(6);
        expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
          5,
          "Received [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.id: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.type: HRT_PPC, certificate.status: ACTIVE, certificate.reference: HRTF44F7E97, certificate.startDate: 2022-12-12, certificate.endDate: 2023-12-12]",
        );
        expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
          6,
          "Notification created successfully for [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx & certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
        );
      },
    );

    it("should process the sqs record and not POST the notification for channel BATCH", async () => {
      // given
      userPreferenceRecord._meta.channel = Channel.BATCH;

      // when
      await expect(processRecord(sqsRecord)).resolves.not.toThrowError();

      // then
      expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
      expect(
        findUserPreferenceForCitizenSpy.mock.calls[0][0],
      ).toMatchInlineSnapshot(expectedUserPreferenceCall);
      expect(sendNotificationPostSpy).toBeCalledTimes(0);
      expect(sendNotificationPostSpy).not.toBeCalled();
      expect(winstonInfoLoggerSpy).toBeCalledTimes(6);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        5,
        "Received [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.id: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.type: HRT_PPC, certificate.status: ACTIVE, certificate.reference: HRTF44F7E97, certificate.startDate: 2022-12-12, certificate.endDate: 2023-12-12]",
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        6,
        "Email/Letter notification is not required for [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx & certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
      );
    });

    it("should process the sqs record and not POST the notification when undefined preference", async () => {
      // given
      userPreferenceRecord.preference = undefined;

      // when
      await expect(processRecord(sqsRecord)).resolves.not.toThrowError();

      // then
      expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
      expect(
        findUserPreferenceForCitizenSpy.mock.calls[0][0],
      ).toMatchInlineSnapshot(expectedUserPreferenceCall);
      expect(sendNotificationPostSpy).toBeCalledTimes(0);
      expect(sendNotificationPostSpy).not.toBeCalled();
    });

    it("should process the sqs record and error when 500 error returned by notification api", async () => {
      // given
      userPreferenceRecord.preference = Preference.EMAIL;
      userPreferenceRecord._meta.channel = Channel.ONLINE;

      sendNotificationPostSpy = jest
        .spyOn(notification, "sendNotificationPost")
        .mockRejectedValueOnce({
          status: 500,
        });

      // when
      await expect(processRecord(sqsRecord)).rejects.toEqual({
        status: 500,
      });

      // then
      expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
      expect(
        findUserPreferenceForCitizenSpy.mock.calls[0][0],
      ).toMatchInlineSnapshot(expectedUserPreferenceCall);
      expect(sendNotificationPostSpy).toBeCalledTimes(1);
      expect(sendNotificationPostSpy.mock.calls[0]).toMatchInlineSnapshot(
        buildExpectedNotificationCall(),
      );
      expect(winstonInfoLoggerSpy).toBeCalledTimes(6);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        5,
        "Received [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.id: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.type: HRT_PPC, certificate.status: ACTIVE, certificate.reference: HRTF44F7E97, certificate.startDate: 2022-12-12, certificate.endDate: 2023-12-12]",
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        6,
        "Notification creation unsuccessful for [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx & certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
      );
    });
  });

  describe("with event as ANY and preference as POSTAL", () => {
    it.each([Channel.PHARMACY, Channel.STAFF])(
      "should process the sqs record and send notification for channel %p",
      async (channel) => {
        // given
        userPreferenceRecord.preference = Preference.POSTAL;
        userPreferenceRecord._meta.channel = channel;

        // when
        await expect(processRecord(sqsRecord)).resolves.not.toThrowError();

        // then
        expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
        expect(
          findUserPreferenceForCitizenSpy.mock.calls[0][0],
        ).toMatchInlineSnapshot(expectedUserPreferenceCall);
        expect(sendNotificationPostSpy).toBeCalledTimes(1);
        expect(sendNotificationPostSpy.mock.calls[0]).toMatchInlineSnapshot(
          buildExpectedNotificationCall("POST", channel),
        );
        expect(winstonInfoLoggerSpy).toBeCalledTimes(6);
        expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
          5,
          "Received [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.id: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.type: HRT_PPC, certificate.status: ACTIVE, certificate.reference: HRTF44F7E97, certificate.startDate: 2022-12-12, certificate.endDate: 2023-12-12]",
        );
        expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
          6,
          "Notification created successfully for [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx & certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
        );
      },
    );

    it.each([Channel.ONLINE, Channel.BATCH])(
      "should process the sqs record and not post to notification for channel %p",
      async (channel) => {
        // given
        userPreferenceRecord.preference = Preference.POSTAL;
        userPreferenceRecord._meta.channel = channel;

        // when
        await expect(processRecord(sqsRecord)).resolves.not.toThrowError();

        // then
        expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
        expect(
          findUserPreferenceForCitizenSpy.mock.calls[0][0],
        ).toMatchInlineSnapshot(expectedUserPreferenceCall);
        expect(sendNotificationPostSpy).toBeCalledTimes(0);
        expect(sendNotificationPostSpy).not.toBeCalled();
        expect(winstonInfoLoggerSpy).toBeCalledTimes(6);
        expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
          5,
          "Received [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.id: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.type: HRT_PPC, certificate.status: ACTIVE, certificate.reference: HRTF44F7E97, certificate.startDate: 2022-12-12, certificate.endDate: 2023-12-12]",
        );
        expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
          6,
          "Email/Letter notification is not required for [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx & certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
        );
      },
    );

    it("should log unsuccessful message and throw error if notification API throws an error", async () => {
      // given
      userPreferenceRecord.preference = Preference.POSTAL;
      userPreferenceRecord._meta.channel = Channel.PHARMACY;
      sendNotificationPostSpy = jest
        .spyOn(notification, "sendNotificationPost")
        .mockRejectedValue(new Error("Internal server error"));

      // when / then
      await expect(processRecord(sqsRecord)).rejects.toEqual(
        new Error("Internal server error"),
      );

      expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
      expect(
        findUserPreferenceForCitizenSpy.mock.calls[0][0],
      ).toMatchInlineSnapshot(expectedUserPreferenceCall);
      expect(sendNotificationPostSpy).toBeCalledTimes(1);
      expect(sendNotificationPostSpy.mock.calls[0]).toMatchInlineSnapshot(
        buildExpectedNotificationCall("POST", Channel.PHARMACY),
      );
      expect(winstonInfoLoggerSpy).toBeCalledTimes(6);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        5,
        "Received [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.id: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.type: HRT_PPC, certificate.status: ACTIVE, certificate.reference: HRTF44F7E97, certificate.startDate: 2022-12-12, certificate.endDate: 2023-12-12]",
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        6,
        "Notification creation unsuccessful for [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx & certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
      );
    });
  });

  describe("with event as REMINDER", () => {
    it.each([
      [Preference.EMAIL, Channel.ONLINE],
      [Preference.POSTAL, Channel.ONLINE],
      [Preference.EMAIL, Channel.PHARMACY],
      [Preference.POSTAL, Channel.PHARMACY],
      [Preference.EMAIL, Channel.STAFF],
      [Preference.POSTAL, Channel.STAFF],
      [Preference.EMAIL, Channel.BATCH],
      [Preference.POSTAL, Channel.BATCH],
    ])(
      "should process the sqs record and not send any notification for fulfilment %p with channel %p",
      async (fulfilment, channel) => {
        // given
        userPreferenceRecord.event = Event.REMINDER;
        userPreferenceRecord.preference = fulfilment;
        userPreferenceRecord._meta.channel = channel;

        // when
        await expect(processRecord(sqsRecord)).resolves.not.toThrowError();

        // then
        expect(findUserPreferenceForCitizenSpy).toBeCalledTimes(1);
        expect(
          findUserPreferenceForCitizenSpy.mock.calls[0][0],
        ).toMatchInlineSnapshot(expectedUserPreferenceCall);
        expect(sendNotificationPostSpy).toBeCalledTimes(0);
        expect(sendNotificationPostSpy).not.toBeCalled();
        expect(winstonInfoLoggerSpy).toBeCalledTimes(6);
        expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
          5,
          "Received [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.id: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx, certificate.type: HRT_PPC, certificate.status: ACTIVE, certificate.reference: HRTF44F7E97, certificate.startDate: 2022-12-12, certificate.endDate: 2023-12-12]",
        );
        expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
          6,
          "Email/Letter notification is not required for [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx & certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
        );
      },
    );
  });

  it.each([
    mockNoCitizenId,
    mockNoCertificateId,
    mockNoCertificateType,
    mockNoCertificateStatus,
    mockNoCertificateReference,
    mockNoCertificateStartDate,
    mockNoCertificateEndDate,
  ])(
    `should throw an error if record body is missing any data`,
    async (data) => {
      // given
      sqsRecord.body = JSON.stringify(data);

      // when
      await expect(processRecord(sqsRecord)).rejects.toThrowError(
        new Error(
          "Invalid record body, must contain citizenId, certificateId, type, status, reference, startDate, endDate",
        ),
      );

      // then
      expect(findUserPreferenceForCitizenSpy).not.toBeCalled();
      expect(sendNotificationPostSpy).not.toBeCalled();
      expect(winstonInfoLoggerSpy).toBeCalledTimes(5);
    },
  );

  it("should not call any API if certificate status is not ACTIVE", async () => {
    // given
    const winstonWarnLoggerSpy = jest.spyOn(logger, "warn");
    sqsRecord.body = JSON.stringify({
      citizenId: "citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx",
      certificate: {
        id: "certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx",
        type: "HRT_PPC",
        status: "PENDING",
        reference: "HRTF44F7E97",
        startDate: "2022-12-12",
        endDate: "2023-12-12",
      },
    });

    // when
    await processRecord(sqsRecord);

    // then
    expect(findUserPreferenceForCitizenSpy).not.toBeCalled();
    expect(sendNotificationPostSpy).not.toBeCalled();
    expect(winstonInfoLoggerSpy).toBeCalledTimes(5);
    expect(winstonWarnLoggerSpy).toBeCalledTimes(1);
    expect(winstonWarnLoggerSpy).toHaveBeenNthCalledWith(
      1,
      "Email/Letter notification is not required for [citizenId: citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx & certificateId: certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
    );
  });
});

describe("processRecord() validation errors", () => {
  let winstonErrorLoggerSpy;
  beforeEach(() => {
    // given
    winstonErrorLoggerSpy = jest.spyOn(logger, "error");
  });

  afterEach(() => {
    // then
    expect(findUserPreferenceForCitizenSpy).not.toBeCalled();
    expect(sendNotificationPostSpy).not.toBeCalled();
    expect(winstonErrorLoggerSpy).toBeCalledTimes(1);
  });

  it("should throw an error when sqs record body is unable to parse", async () => {
    // given
    sqsRecord.body = "";

    // when
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      "Unexpected end of JSON input",
    );

    // then
    expect(winstonErrorLoggerSpy).toBeCalledWith(
      "Unable to parse record body: {}",
    );
  });

  it("should not process with any api call when messageAttributes are empty", async () => {
    // given
    sqsRecord.messageAttributes = {};

    // when
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error("Invalid SQS message attributes, must contain correlationId"),
    );

    // then
    expect(winstonErrorLoggerSpy).toBeCalledWith(
      "Invalid SQS message attributes, must contain correlationId",
    );
  });

  it("should not process with any api call when correlationId is missing", async () => {
    // given
    sqsRecord.messageAttributes = {
      eventName: {
        dataType: "string",
        stringValue: "certificateStatusChange",
      },
      source: {
        dataType: "string",
        stringValue: "health-charge-exemption-certificate-api",
      },
      eventType: {
        dataType: "string",
        stringValue: "issue-certificate",
      },
    };

    // when
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error("Invalid SQS message attributes, must contain correlationId"),
    );

    // then
    expect(winstonErrorLoggerSpy).toBeCalledWith(
      "Invalid SQS message attributes, must contain correlationId",
    );
  });

  it("should not process with any api call when stringValue is missing", async () => {
    // given
    sqsRecord.messageAttributes = {
      eventName: {
        dataType: "string",
        stringValue: "certificateStatusChange",
      },
      source: {
        dataType: "string",
        stringValue: "health-charge-exemption-certificate-api",
      },
      eventType: {
        dataType: "string",
        stringValue: "issue-certificate",
      },
      correlationId: {
        dataType: "string",
      },
    };

    // when
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error("Invalid SQS message attributes, must contain correlationId"),
    );

    // then
    expect(winstonErrorLoggerSpy).toBeCalledWith(
      "Invalid SQS message attributes, must contain correlationId",
    );
  });

  it("should not process with any api call when stringValue is empty", async () => {
    // given
    sqsRecord.messageAttributes = {
      eventName: {
        dataType: "string",
        stringValue: "certificateStatusChange",
      },
      source: {
        dataType: "string",
        stringValue: "health-charge-exemption-certificate-api",
      },
      eventType: {
        dataType: "string",
        stringValue: "issue-certificate",
      },
      correlationId: {
        dataType: "string",
        stringValue: "",
      },
    };

    // when
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error("Invalid SQS message attributes, must contain correlationId"),
    );

    // then
    expect(winstonErrorLoggerSpy).toBeCalledWith(
      "Invalid SQS message attributes, must contain correlationId",
    );
  });
});
