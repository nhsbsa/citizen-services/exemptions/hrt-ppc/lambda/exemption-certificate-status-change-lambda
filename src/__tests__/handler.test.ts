import { SQSEvent } from "aws-lambda";
import { handler } from "../index";
import { processRecord } from "../process-sqs-event-record";
import mockEventSingleRecord from "../../events/event-single-record-success.json";
import mockEventMultipleRecords from "../../events/event-multiple-records.json";

jest.mock("../process-sqs-event-record");

let event: SQSEvent;
const mockProcessRecord = processRecord as jest.MockedFunction<
  typeof processRecord
>;

beforeEach(() => {
  jest.useFakeTimers().setSystemTime(new Date("2020-01-01"));
  event = JSON.parse(JSON.stringify(mockEventSingleRecord));
  mockProcessRecord.mockClear();
  mockProcessRecord.mockResolvedValue();
});

describe("handler", () => {
  it("should respond with an internal server error on single record", async () => {
    const error = Error("Internal Server Error");
    mockProcessRecord.mockRejectedValueOnce(error);

    await expect(() =>
      handler(event),
    ).rejects.toThrowErrorMatchingInlineSnapshot(`"Internal Server Error"`);
    expect(mockProcessRecord).toBeCalledTimes(1);
  });

  it("should respond with an internal server error on 2nd record in multiple records ", async () => {
    event = JSON.parse(JSON.stringify(mockEventMultipleRecords));
    const error = Error("Internal Server Error");
    mockProcessRecord
      .mockResolvedValueOnce(undefined)
      .mockRejectedValueOnce(error)
      .mockResolvedValueOnce(undefined);

    await expect(() =>
      handler(event),
    ).rejects.toThrowErrorMatchingInlineSnapshot(`"Internal Server Error"`);
    expect(mockProcessRecord).toBeCalledTimes(3);
  });

  it("should handle the event successfully without any error", async () => {
    await expect(handler(event)).resolves.not.toThrowError();
    expect(mockProcessRecord).toBeCalledTimes(1);
  });
});
