import {
  CertificateType,
  NotificationMethod,
  NotificationPostResponse,
  NotificationPurpose,
  NotificationStatus,
  UserPreferenceGetResponse,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import {
  Channel,
  Event,
  Preference,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest/dist/src/clients/common";

export const mockSQSRecord = {
  messageId: "059f36b4-87a3-44ab-83d2-661975830a7d",
  receiptHandle: "AQEBwJnKyrHigUMZj6rYigCgxlaS3SLy0a",
  body: '{"citizenId":"citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx","certificate":{"id":"certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx","type":"HRT_PPC","status":"ACTIVE","reference":"HRTF44F7E97","startDate":"2022-12-12","endDate":"2023-12-12"}}',
  attributes: {
    ApproximateReceiveCount: "1",
    SentTimestamp: "1545082649183",
    SenderId: "AIDAIENQZJOLO23YVJ4VO",
    ApproximateFirstReceiveTimestamp: "1545082649185",
  },
  messageAttributes: {
    eventName: {
      dataType: "string",
      stringValue: "certificateStatusChange",
    },
    source: {
      dataType: "string",
      stringValue: "health-charge-exemption-certificate-api",
    },
    eventType: {
      dataType: "string",
      stringValue: "issue-certificate",
    },
    correlationId: {
      dataType: "string",
      stringValue: "a580305a-dae9-46a4-a420-ddee91054d7a",
    },
  },
  md5OfBody: "098f6bcd4621d373cade4e832627b4f6",
  eventSource: "aws:sqs",
  eventSourceARN: "arn:aws:sqs:us-east-2:123456789012:my-queue",
  awsRegion: "us-east-2",
};

export const mockUserPreferenceRecord: UserPreferenceGetResponse = {
  id: "ac14c125-8877-17b0-8188-7727f1570001",
  citizenId: "citizenx-idxx-xxxx-xxxx-xxxxxxxxxxxx",
  certificateType: CertificateType.HRT_PPC,
  event: Event.ANY,
  preference: Preference.EMAIL,
  _meta: {
    channel: Channel.ONLINE,
    createdTimestamp: new Date("2023-06-01T14:31:26.423507"),
    createdBy: "ONLINE",
    updatedTimestamp: new Date("2023-06-01T14:31:26.423507"),
    updatedBy: "ONLINE",
  },
  _links: {
    self: {
      href: "http://localhost:8150/v1/user-preference/ac14c125-8877-17b0-8188-7727f1570001",
    },
    userPreference: {
      href: "http://localhost:8150/v1/user-preference/ac14c125-8877-17b0-8188-7727f1570001",
    },
  },
};

export function mockNotificationRecord(
  method?: NotificationMethod,
): NotificationPostResponse {
  return {
    id: "notifapi-idxx-xxxx-xxxx-xxxxxxxxxxxx",
    certificateId: "certifxx-idxx-xxxx-xxxx-xxxxxxxxxxxx",
    certificateType: CertificateType.HRT_PPC,
    purpose: NotificationPurpose.ISSUE,
    method: method ?? NotificationMethod.EMAIL,
    status: NotificationStatus.PENDING,
    _meta: {
      channel: Channel.ONLINE,
      createdTimestamp: new Date("2023-06-01T14:31:26.423507"),
      createdBy: "ONLINE",
      updatedTimestamp: new Date("2023-06-01T14:31:26.423507"),
      updatedBy: "ONLINE",
    },
    _links: {
      self: {
        href: "http://localhost:8130/v1/notifications/notifapi-idxx-xxxx-xxxx-xxxxxxxxxxxx",
      },
      notification: {
        href: "http://localhost:8130/v1/notifications/notifapi-idxx-xxxx-xxxx-xxxxxxxxxxxx",
      },
    },
  };
}

export const mockEmailResponse = {
  data: {
    id: "mock-email-sent-id",
  },
};

export const mockNoCitizenId = {
  citizenId: "",
  certificate: {
    id: "c0a80865-85ab-1b54-8185-bb3432d90015",
    type: "HRT_PPC",
    status: "ACTIVE",
    reference: "HRTF44F7E97",
    startDate: "2022-12-12",
    endDate: "2023-12-12",
  },
};

export const mockNoCertificateId = {
  citizenId: "c0a80005-859c-1146-8185-b09850de0018",
  certificate: {
    id: "",
    type: "HRT_PPC",
    status: "ACTIVE",
    reference: "HRTF44F7E97",
    startDate: "2022-12-12",
    endDate: "2023-12-12",
  },
};

export const mockNoCertificateType = {
  citizenId: "c0a80005-859c-1146-8185-b09850de0018",
  certificate: {
    id: "c0a80865-85ab-1b54-8185-bb3432d90015",
    type: "",
    status: "ACTIVE",
    reference: "HRTF44F7E97",
    startDate: "2022-12-12",
    endDate: "2023-12-12",
  },
};

export const mockNoCertificateStatus = {
  citizenId: "c0a80005-859c-1146-8185-b09850de0018",
  certificate: {
    id: "c0a80865-85ab-1b54-8185-bb3432d90015",
    type: "HRT_PPC",
    status: "",
    reference: "HRTF44F7E97",
    startDate: "2022-12-12",
    endDate: "2023-12-12",
  },
};

export const mockNoCertificateReference = {
  citizenId: "c0a80005-859c-1146-8185-b09850de0018",
  certificate: {
    id: "c0a80865-85ab-1b54-8185-bb3432d90015",
    type: "HRT_PPC",
    status: "ACTIVE",
    reference: "",
    startDate: "2022-12-12",
    endDate: "2023-12-12",
  },
};

export const mockNoCertificateStartDate = {
  citizenId: "c0a80005-859c-1146-8185-b09850de0018",
  certificate: {
    id: "c0a80865-85ab-1b54-8185-bb3432d90015",
    type: "HRT_PPC",
    status: "ACTIVE",
    reference: "HRTF44F7E97",
    startDate: "",
    endDate: "2023-12-12",
  },
};

export const mockNoCertificateEndDate = {
  citizenId: "c0a80005-859c-1146-8185-b09850de0018",
  certificate: {
    id: "c0a80865-85ab-1b54-8185-bb3432d90015",
    type: "HRT_PPC",
    status: "ACTIVE",
    reference: "HRTF44F7E97",
    startDate: "2022-12-12",
    endDate: "",
  },
};
