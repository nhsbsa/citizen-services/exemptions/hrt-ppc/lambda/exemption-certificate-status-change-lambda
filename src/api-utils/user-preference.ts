import {
  UserPreferenceApi,
  UserPreferenceGetResponse,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

const userPreferenceApi = new UserPreferenceApi();

export async function findUserPreferenceForCitizen({
  certificateType,
  citizenId,
  headers,
}): Promise<UserPreferenceGetResponse> {
  return await userPreferenceApi.makeRequest({
    method: "GET",
    url: "/v1/user-preference/search/findByCitizenIdAndCertificateType",
    params: { citizenId, certificateType },
    headers,
    responseType: "json",
  });
}
