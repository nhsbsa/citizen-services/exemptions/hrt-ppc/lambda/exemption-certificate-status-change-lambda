import { findUserPreferenceForCitizen } from "../user-preference";
import { UserPreferenceApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { mockUserPreferenceRecord } from "../../__mocks__/mockData";

let userPreferenceApiSpy: jest.SpyInstance;
const certificateType = "HRT_PPC";
const citizenId = "citizenId";
const headers = {
  header: "value",
};

beforeEach(() => {
  userPreferenceApiSpy = jest
    .spyOn(UserPreferenceApi.prototype, "makeRequest")
    .mockResolvedValue({});
});
afterEach(() => {
  jest.resetAllMocks();
});

describe("userPreference", () => {
  describe("findUserPreferenceForCitizen()", () => {
    it("should return the response successfully", async () => {
      userPreferenceApiSpy.mockResolvedValue(mockUserPreferenceRecord);
      const response = await findUserPreferenceForCitizen({
        certificateType,
        citizenId,
        headers,
      });
      expect(response).toBe(mockUserPreferenceRecord);
    });

    it("should call the user preference api with correct parameters", async () => {
      await findUserPreferenceForCitizen({
        certificateType,
        citizenId,
        headers,
      });
      expect(userPreferenceApiSpy).toHaveBeenCalledTimes(1);
      expect(userPreferenceApiSpy.mock.calls[0]).toMatchInlineSnapshot(`
        [
          {
            "headers": {
              "header": "value",
            },
            "method": "GET",
            "params": {
              "certificateType": "HRT_PPC",
              "citizenId": "citizenId",
            },
            "responseType": "json",
            "url": "/v1/user-preference/search/findByCitizenIdAndCertificateType",
          },
        ]
      `);
    });
  });
});
