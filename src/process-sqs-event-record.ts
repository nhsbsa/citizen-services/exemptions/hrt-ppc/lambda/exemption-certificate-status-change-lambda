import {
  loggerWithContext,
  setCorrelationId,
} from "@nhsbsa/hrt-ppc-npm-logging";
import { SQSRecord } from "aws-lambda";
import {
  findUserPreferenceForCitizen,
  HEADER_CPS_EVENT,
  sendNotificationPost,
} from "./api-utils";
import {
  Channel,
  Preference,
  Event,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest/dist/src/clients/common";
import {
  NotificationMethod,
  NotificationPostRequestBody,
  NotificationPurpose,
  UserPreferenceGetResponse,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

let logger = loggerWithContext();

// Exemption certificate status
const ExemptionCertificateStatus = {
  ACTIVE: "ACTIVE",
} as const;

/**
 * Map of allowed enum pairs.
 */
const channelMap: Record<Preference, Channel[]> = {
  [Preference.EMAIL]: [Channel.ONLINE, Channel.PHARMACY, Channel.STAFF],
  [Preference.POSTAL]: [Channel.STAFF, Channel.PHARMACY],
};

export async function processRecord(record: SQSRecord) {
  logger.info(`Processing event [messageId: ${record.messageId}]`);

  const correlationId = validMessageAttributes(record);
  const recordBody = validateRecordBody(record);

  const { citizenId, certificate } = recordBody;
  const { certificateId, status } = certificate;
  const headers = buildHeaders(HEADER_CPS_EVENT, correlationId);

  if (status !== ExemptionCertificateStatus.ACTIVE) {
    logger.warn(
      `Email/Letter notification is not required for [citizenId: ${citizenId} & certificateId: ${certificateId}]`,
    );
    return;
  }

  const userPreferenceResponse = await findUserPreferenceForCitizen({
    certificateType: certificate.type,
    citizenId,
    headers,
  });

  await sendNotification(
    userPreferenceResponse,
    certificate,
    citizenId,
    headers,
  );
}

/**
 * Builds a POST request for the Notification API after logical checks on preference data {@link UserPreferenceResponse}.
 *
 * @param userPreferenceResponse user preference response object
 * @param certificate certificate response object
 * @param headers headers to be sent to the notification api
 * @returns void
 */
async function sendNotification(
  userPreferenceResponse: UserPreferenceGetResponse,
  certificate,
  citizenId,
  headers,
): Promise<void> {
  const { event, preference, _meta } = userPreferenceResponse;
  const { certificateId, type } = certificate;
  const noNotificationMessage = `Email/Letter notification is not required for [citizenId: ${citizenId} & certificateId: ${certificateId}]`;
  if (event !== Event.ANY) {
    logger.info(noNotificationMessage);
    return;
  }

  const shouldSendNotification: boolean =
    channelMap[preference]?.includes(_meta.channel) ?? false;

  if (shouldSendNotification) {
    const data: NotificationPostRequestBody = {
      purpose: NotificationPurpose.ISSUE,
      method:
        preference.toUpperCase() === Preference.POSTAL
          ? NotificationMethod.POST
          : NotificationMethod.EMAIL,
      certificateType: type,
    };
    await sendNotificationPost(
      certificateId,
      {
        ...headers,
        channel: _meta.channel,
      },
      data,
    )
      .then(() => {
        logger.info(
          `Notification created successfully for [citizenId: ${citizenId} & certificateId: ${certificateId}]`,
        );
      })
      .catch((error) => {
        logger.info(
          `Notification creation unsuccessful for [citizenId: ${citizenId} & certificateId: ${certificateId}]`,
        );
        throw error;
      });
  } else {
    logger.info(noNotificationMessage);
  }
}

function buildHeaders(userId: string, correlationId: string) {
  return {
    channel: Channel.BATCH,
    "user-id": userId,
    "correlation-id": correlationId,
  };
}

function validMessageAttributes(record: SQSRecord) {
  logger.info(`Validating SQS message attributes`);

  const { eventName, source, eventType, correlationId } =
    record.messageAttributes;
  const eventNameValue = eventName?.stringValue;
  const sourceValue = source?.stringValue;
  const eventTypeValue = eventType?.stringValue;
  const correlationIdValue = correlationId?.stringValue;

  logger.info(
    `Received [eventName: ${eventNameValue}, source: ${sourceValue}, eventType: ${eventTypeValue} correlationId: ${correlationIdValue}]`,
  );
  if (!correlationIdValue) {
    const message =
      "Invalid SQS message attributes, must contain correlationId";
    logger.error(message);
    throw new Error(message);
  }
  setCorrelationId(correlationIdValue);
  logger = loggerWithContext();
  return correlationIdValue;
}

function validateRecordBody(record: SQSRecord) {
  try {
    logger.info(`Validating record body`);
    const {
      citizenId,
      certificate: {
        id: certificateId,
        type,
        status,
        reference,
        startDate,
        endDate,
      },
    } = JSON.parse(record.body);

    logger.info(
      `Received [citizenId: ${citizenId}, certificate.id: ${certificateId}, certificate.type: ${type}, certificate.status: ${status}, certificate.reference: ${reference}, certificate.startDate: ${startDate}, certificate.endDate: ${endDate}]`,
    );

    if (
      !citizenId ||
      !certificateId ||
      !type ||
      !status ||
      !reference ||
      !startDate ||
      !endDate
    ) {
      const message = `Invalid record body, must contain citizenId, certificateId, type, status, reference, startDate, endDate`;
      logger.error(message);
      throw new Error(message);
    }

    return {
      citizenId,
      certificate: {
        certificateId,
        type,
        status,
        reference,
        startDate,
        endDate,
      },
    };
  } catch (err) {
    const message = `Unable to parse record body: ${JSON.stringify(err)}`;
    logger.error(message);
    throw err;
  }
}
